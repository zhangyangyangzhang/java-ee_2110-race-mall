package com.qf.pojo;

/**
 * @author Kuke
 * @date 2021/11/26 9:45
 * 定义购物车项实体
 */
public class CartItem {

    private Product product ;//包含商品实体
    private int count ; //商品数量
    private Double subTotal = 0.0 ;//小计金额

    public CartItem() {
    }

    public CartItem(Product product, int count) {
        this.product = product;
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /*
    * 获取小计金额:商品的数量乘以商品单价即可
    * */
    public Double getSubTotal() {
        return this.count * product.getShop_price();
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "product=" + product +
                ", count=" + count +
                ", subTotal=" + subTotal +
                '}';
    }
}

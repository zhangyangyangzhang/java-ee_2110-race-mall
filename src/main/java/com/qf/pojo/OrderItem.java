package com.qf.pojo;

/**
 * @author Kuke
 * @date 2021/11/26 16:05
 * 订单项实体
 */
public class OrderItem {
    private String itemid; //订单项id
    private int count ; //订单项中的商品的数量
    private Double subtotal = 0.0 ;//订单项中的小计金额

    //包含的商品
    private Product product ;
    //这个订单项属于哪一个订单
    private Order order ;

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    //计算出来
    public Double getSubtotal() {
        return this.product.getShop_price()* this.count;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "itemid='" + itemid + '\'' +
                ", count=" + count +
                ", subtotal=" + subtotal +
                ", product=" + product +
                ", order=" + order +
                '}';
    }
}

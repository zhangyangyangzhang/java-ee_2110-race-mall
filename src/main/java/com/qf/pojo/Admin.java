package com.qf.pojo;

/**
 * @author Kuke
 * @date 2021/11/17 11:35
 * 管理员的实体
 */
public class Admin {
    /**
     * CREATE TABLE admin(
     * 	id INT PRIMARY KEY AUTO_INCREMENT, -- 编号
     * 	NAME VARCHAR(10), -- 管理员名称
     * 	PASSWORD VARCHAR(10) -- 管理员密码
     *
     * ) ;
     */
    private int id ;
    private String adminname ;
    private String adminpassword ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getAdminname() {
        return adminname;
    }

    public void setAdminname(String adminname) {
        this.adminname = adminname;
    }

    public String getAdminpassword() {
        return adminpassword;
    }

    public void setAdminpassword(String adminpassword) {
        this.adminpassword = adminpassword;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", adminname='" + adminname + '\'' +
                ", adminpassword='" + adminpassword + '\'' +
                '}';
    }
}

package com.qf.pojo;

import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/20 10:47
 * 后台的分页组件
 */
public class PageBean<T> {
    private int currentPage ;//当前页码
    private int pageSize ; //每页显示的条数
    private int totalCount ; //总记录数
    private int totalPage ;//总页数
    private List<T> list ; //分页查询的列表数据


    public PageBean() {
    }

    /*
    * 只提供四个参数即可,totalPage计算出来的
    * */
    public PageBean(int currentPage, int pageSize, int totalCount,  List<T> list) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        this.list = list;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 总页数可以计算出来的
     * @return
     */
    public int getTotalPage() {
       // return (int) Math.ceil(totalCount/pageSize);
        return ((totalCount%pageSize==0)?totalCount/pageSize:totalCount/pageSize+1) ;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "currentPage=" + currentPage +
                ", pageSize=" + pageSize +
                ", totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", list=" + list +
                '}';
    }
}


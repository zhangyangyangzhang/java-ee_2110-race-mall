package com.qf.pojo;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/26 16:07
 *
 * 订单实体
 */
public class Order {

    private String oid; //订单的编号

    private LocalDateTime ordertime;//下订单时间 时间格式
    private Double total;//总计金额
    private int state;//订单的状态 0,未支付 ,1,已支付
    private String address;//下单地址
    private String name;//下订单的用户名
    private String telephone; //下订单的联系方式

    //这个订单属于哪个用户的
    private User user;

    //订单中包含很多多个订单项,每一个订单项---就是OrderItem
    private List<OrderItem> items = new LinkedList<>() ;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public LocalDateTime getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(LocalDateTime ordertime) {
        this.ordertime = ordertime;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

   /* @Override
    public String toString() {
        return "Order{" +
                "oid='" + oid + '\'' +
                ", ordertime=" + ordertime +
                ", total=" + total +
                ", state=" + state +
                ", address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", telephone='" + telephone + '\'' +
                ", user=" + user +
                ", items=" + items +
                '}';
    }*/
}

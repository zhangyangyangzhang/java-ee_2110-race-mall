package com.qf.pojo;

/**
 * @author Kuke
 * @date 2021/11/24 14:36
 * 分类实体
 */
public class Category {

  /*  cid varchar(32) NOT NULL
    cname varchar(20) NULL*/
    private String cid ;//分类名称
    private String cname ;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    @Override
    public String toString() {
        return "Category{" +
                "cid='" + cid + '\'' +
                ", cname='" + cname + '\'' +
                '}';
    }
}

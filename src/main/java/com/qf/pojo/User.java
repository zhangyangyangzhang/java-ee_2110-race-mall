package com.qf.pojo;

import java.util.Date;

/**
 * @author Kuke
 * @date 2021/11/18 14:20
 * 用户实体类
 */
public class User {
    /**
     * `uid` varchar(32) NOT NULL,               UUID生成随机32字符
     *   `username` varchar(20) DEFAULT NULL,
     *   `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
     *   `name` varchar(20) DEFAULT NULL,
     *   `email` varchar(30) DEFAULT NULL,
     *   `telephone` varchar(20) DEFAULT NULL,
     *   `birthday` date DEFAULT NULL,
     *   `sex` varchar(10) DEFAULT NULL,
     *   `state` int DEFAULT NULL,
     *   `code` varchar(64) DEFAULT NULL,
     */
    private String uid ;//用户编号
    private String username ; //用户网名
    private String password ;//密码
    private String name ;//真实姓名
    private String email ; //邮箱
    private String telephone ; //电话号码
    private Date birthday ; //出生日期
    private String sex ; //性别
    private int state =0; //用户激活状态:0表示未激活 1,表示已激活
    private String code ;//激活码  可以通过UUID工具生成随机字符串

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", birthday=" + birthday +
                ", sex='" + sex + '\'' +
                ", state=" + state +
                ", code='" + code + '\'' +
                '}';
    }
}

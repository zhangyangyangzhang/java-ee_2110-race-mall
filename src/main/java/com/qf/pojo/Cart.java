package com.qf.pojo;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Kuke
 * @date 2021/11/26 9:48
 * 定义购物车实体----以后放在HttpSession中,存储到服务器 setAttribute("cart",cart购物车实体)
 */
public class Cart {

    //基本信息
    //包含很多个购物车项
    //Key:商品pid
    //Value:购物车项
    private Map<String,CartItem> map = new LinkedHashMap<>() ;// 保证迭代次序有序
    //总计金额
    private Double totalMoney = 0.0 ;

    //Collection<V> values() Map的功能:获取所有的值集合
    public Collection<CartItem> getItems() { //getItems()    ---items:就是Cart的bean属性
            return   map.values() ;
    }

    public Map<String, CartItem> getMap() {
        return map;
    }

    public void setMap(Map<String, CartItem> map) {
        this.map = map;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }


    //封装一个功能:加入购物车功能  将购物车项加入购物车
    public void addCartItem2Cart(CartItem cartItem){
            //通过购物车项获取它的商品编号
        String pid = cartItem.getProduct().getPid();
        //在Map中判断pid是否重复
        if(map.containsKey(pid)){
            //重复了
            //通过当前pid获取它以前的购物车项
            CartItem oldCartItem = map.get(pid);
            //设置以前的购物车项商品数量
            oldCartItem.setCount(oldCartItem.getCount()+cartItem.getCount());

        }else{
            //购物车项不重复
            //给map集合中添加新的购物车项
            map.put(pid,cartItem) ;
        }

        totalMoney += cartItem.getSubTotal() ;
    }



    /**
     * 将购物车项从购物车中删除
     * @param pid 商品编号
     */
    public void removeCarItemFromCart(String pid){
        //通过商品pid删除对应的购物车项,返回被删除对应的值
        CartItem cartItem = map.remove(pid);
        totalMoney -= cartItem.getSubTotal() ;

    }

    /**
     * 清空购物车
     */
    public void clearCart(){
        map.clear();
        totalMoney = 0.0 ;
    }

}

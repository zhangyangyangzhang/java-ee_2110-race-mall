package com.qf.config;

/**
 * @author Kuke
 * @date 2021/9/11
 *   支付宝的核心配置类  里面设置支付宝账户的基本信息 必填的
 *
 */
public class AlipayConfig {
    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数
    public static String return_url = "http://m8eju4.natappfree.cc/aliPayReturnUrlServlet";  // 此处使用外网ip(使用免费的花生壳或者netapp产生的外网域名)
    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://m8eju4.natappfree.cc/notify";
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016110100783811";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnWopbCdRv3WcwVY29Y4+kV3YbVHjz5k2+r8dQWoT9FP19gQAC1OhdypYnptEVIPevm9b2SfJxp0V/cnWFZxPpmCPzDmcKmj6iuVFw/lxNbqV7gjVtHSH7+J1jKFqg7u0u3WsUszXqOFbTB/dWBPHjL1T7WvVjXJupcL9xD306pkt5pFkzcllsdqRbwVQEa1zWftEWatVBiEAnAuTC3Kerf/xSyMn1WRMgzooyO2Lft45MvDq2LUT1d99BK6HQilrXkBUWPJ8yurD2Pv0uTlaDpUyqb2cnKrRqfB1KFAksftGsmRrb0xlRYiDIqluhC6jATMj2NTSGSwkGxbTpGEpPQIDAQAB";
    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkK1CWkU/+RC5WdQ88MGimv+EBOzW+EgKDY/Cj6evoDaoPTEHeXHG6t2AqnhhcMWNBJzhz6jIqW0eNrgxktVnA4HGeJ5otZdAK5Bufms7tXrvyRuXZwCoZ0lmyVLpq5mlMM7qSHrsN1KM1Tsd4HsrYHeUzk37fdKAwF/Jvwsbku1l4bSdl+QHmHd92MuE4zFOqhODpta6XrclWrencemOy00RbeYDiJY9/r0hQ1W+cInNXk7uGylc3ruqA/ya4pLzAxBSm0R/Y/EjdSd7ckP9hk2aoj377ygSKdr8RHq01xI1j5INhKtbzCdygRKGNjeks9SPVDhizUs2qBAHqBYV1AgMBAAECggEALZ2jfNLPqVUEkx1fqFfDHGJAgPkUy8VxUVTZrpH27LmvQSTtQH3rU/hUDkNzfVpzu8zMENlIKyDF1wY/h1QkpkU3DLzwSCD8F6qeSlDXonkqO/2bLFAK4rXiGn0Ud1Oienv9IVDMpOEqoEtNfYEA1+AmqZgYLrrXL8XXpVo3raI27vYBkptVfrYBZynkV+R94GPU2NUkmwHq1HFRIAwt1meuMtjDq4x6PpMXJaxzbLCG2h1E9QPettql92mViipyazG0TMJCgqORuTv7GaWqeqsxBDxf2BJMFuy1lGDO/OsRYOG87K383TLCGYNhqhs7ottpCqCtJ24hNTRHzkDrAQKBgQDZmY8/h1FrSk31HJoi1sxsfDB63F5q71JTQunhpCDxvDZ33fsKxPdCRbtu3YpawejKoxyGBzktOI6/ZJynWDwcSsNfz5mtXOmCT7aL5CXaB7a6jsDIjvmYHFKKYCSM08kbVN8zpTYc2SWueE+tIRKdWMuT/C2puD4A0L8bmSaQFQKBgQDBI++G+NF6+DWx/P23eB2RsdWTgcyaHQ+pYnYn3CpaDmCtagYdp8O+S7tBRggQiNtkHrdFb7KgW0WqBwz7bgRpYswp95lNeIeQ7Hj5bMDmMmQkQBUdb/YRtaFYTK4qMv+3DpyGN8vEizSv/m+qmOSYh3IwI2rTWtsYqPG3lDMX4QKBgFaBi8dPvniO06ssMcLD7gT64jMVxJtyhSIAqgZAP7CaiHTi1NiVMU8xdbA/dq9aA3f7nG1UFdD6eRVxpC2LEGqW9lDysT+QfmyF0CaRDtM82dIUymNxI5zrNY05Z45s67hn50wa1AZn9WkE/3fue/39vQEZ8fVU/6FHW5JXMlN5AoGBAL0ma3OdBNLQeQ26PeRLj3HbXtZaXT1lTqxNkHfTB+swf3/2AM1UOsCIPHhdlJJU5b+00ToAfUjMenpc81rd0RZAusAzC4EylFnbLVLhLj2Dhk12MPemaa/IWJ44Yq1TStpFRgewZzAyQMRHg6bom5/BrpP0a69YIJCTDD8EtdEhAoGBAIYj3ihxr7LLtFF+IkEpB6suHhK2/Y3F2WbV//UYCkDBl8XyrzOi6Jri3nH5NEDnPeLXabNi5JfAAGEYHCb4RosdJz1chsgnrN+h9zz5Bw234h267zvjh0X4L1PmQqejEVzTVpcpLS0FHGIQ6kqN/iLgQMGWM0+6WYezSVWNGslj" ;
    public static String sign_type = "RSA2";
    // 字符编码格式
    public static String charset = "utf-8";
    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do" ;
}

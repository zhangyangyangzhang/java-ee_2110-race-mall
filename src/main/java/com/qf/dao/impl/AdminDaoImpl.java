package com.qf.dao.impl;

import com.qf.dao.AdminDao;
import com.qf.pojo.Admin;
import com.qf.utils.DruidJbdcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

/**
 * @author Kuke
 * @date 2021/11/17 11:49
 * 针对管理员的数据接口实现层
 */
public class AdminDaoImpl implements AdminDao {
    @Override
    public Admin selectAdminByName(String name) throws SQLException {
        //创建执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "select * from admin where adminname = ?" ;
        //查询
        Admin admin = qr.query(sql, new BeanHandler<Admin>(Admin.class), name);
        return admin;
    }
}

package com.qf.dao.impl;

import com.qf.dao.ProductDao;
import com.qf.pojo.Product;
import com.qf.utils.DruidJbdcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/25 10:47
 * 针对商品的数据访问接口实现
 */
public class ProductDaoImpl  implements ProductDao {
    /**
     * 查询最新商品
     * @return 返回商品列表,限制9条记录
     */
    @Override
    public List<Product> selectNewProduct() throws SQLException {
        //执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "select * from product  order by pdate desc limit 9 " ;
        //执行查询
        List<Product> list = qr.query(sql, new BeanListHandler<Product>(Product.class));
        return list;
    }
    /**
     * 查询热门商品
     * @return 返回商品列表,限制9条记录
     */
    @Override
    public List<Product> selectHotProduct() throws SQLException {
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        String sql = "select * from product where is_hot =1  order by pdate desc limit 9" ;
        List<Product> list = qr.query(sql, new BeanListHandler<Product>(Product.class));
        return list ;
    }

    /**
     * 数据访问接口:通过商品编号查询商品
     * @param pid 商品编号
     * @return 返回商品实体
     */
    @Override
    public Product selectProductByPid(String pid) throws SQLException {
        //执行对象
        QueryRunner qr  = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "select * from product where pid = ?" ;
        //执行
        Product product = qr.query(sql, new BeanHandler<Product>(Product.class), pid);
        return  product;
    }

    /**
     * 获取商品的分类的列表数据
     * @param currentPage 当前页码
     * @param pageSize  每页显示的条数
     * @param cid  分类编号
     * @return  分页列表数据
     */
    @Override
    public List<Product> selectProductByPage(int currentPage, int pageSize, String cid) throws SQLException {
        //执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql  = "select * from product where cid = ? limit ?,?" ;
        //执行查询
        List<Product> list = qr.query(sql, new BeanListHandler<Product>(Product.class), cid, (currentPage - 1) * pageSize, pageSize);
        return list;
    }

    /**
     * 通过分类编号查询总记录数
     * @param cid 分类编号
     * @return 返回总记录数
     */
    @Override
    public int selectTotalCount(String cid) throws SQLException {
        //执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "select count(pid) from product  where cid = ?" ;
        Object obj = qr.query(sql, new ScalarHandler<>(), cid);
        String s = String.valueOf(obj);
        int totalCount = Integer.parseInt(s);
        return totalCount;
    }
}

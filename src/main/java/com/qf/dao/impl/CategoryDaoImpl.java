package com.qf.dao.impl;

import com.qf.dao.CategoryDao;
import com.qf.pojo.Category;
import com.qf.utils.DruidJbdcUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/24 14:42
 * 分类的数据访问接口实现
 */
public class CategoryDaoImpl implements CategoryDao {
    /**
     * 分类数据访问接口获取所有分类信息
     * @return 分类的列表
     */
    @Override
    public List<Category> selectAllCategory() throws SQLException {
        //执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "select * from category" ;
        List<Category> list = qr.query(sql, new BeanListHandler<Category>(Category.class));
        return list;
    }
}

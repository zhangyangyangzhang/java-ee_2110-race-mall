package com.qf.dao.impl;

import com.alipay.api.domain.QRcode;
import com.qf.dao.OrderDao;
import com.qf.pojo.Order;
import com.qf.pojo.OrderItem;
import com.qf.pojo.Product;
import com.qf.utils.DruidJbdcUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Kuke
 * @date 2021/11/26 17:24
 * 订单数据访问接口实现
 */
public class OrderDaoImpl implements OrderDao {
    /**
     * 插入订单数据
     * @param order 订单实体
     */
    @Override
    public void insertOrder(Order order) throws SQLException {

        //属于自定提交
        //手动提交
        //QueryRunner():不能带数据源
        //执行update(Connection conn,String sql, Object...params) ;

        //执行对象
//        QueryRunner qr  = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        /*String sql = "insert into orders values(?,?,?,?,?,?,?,?)" ;
        int count = qr.update(sql, order.getOid(),
                order.getOrdertime(),
                order.getTotal(),
                order.getState(),
                order.getAddress(),
                order.getName(),
                order.getTelephone(),
                order.getUser().getUid());
        System.out.println(count);*/

        //手动提交
        QueryRunner qr  = new QueryRunner() ;
       //sql
        String sql = "insert into orders values(?,?,?,?,?,?,?,?)" ;
        int count = qr.update(DruidJbdcUtils.getConnetion(), sql,
                order.getOid(),
                order.getOrdertime(),
                order.getTotal(),
                order.getState(),
                order.getAddress(),
                order.getName(),
                order.getTelephone(),
                order.getUser().getUid());
        System.out.println(count);


    }

    /**
     * 插入订单项数据
     * @param orderItem 订单实体
     */
    @Override
    public void insertOrderItem(OrderItem orderItem) throws SQLException {
        //执行对象
       /* QueryRunner qr  = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "insert into orderitem values(?,?,?,?,?)" ;
        int count = qr.update(sql,
                orderItem.getItemid(),
                orderItem.getCount(),
                orderItem.getSubtotal(),
                orderItem.getProduct().getPid(),
                orderItem.getOrder().getOid()) ;
        System.out.println(count);*/

       //手动提交
        QueryRunner qr  = new QueryRunner() ;
        String sql = "insert into orderitem values(?,?,?,?,?)" ;
        int count = qr.update(DruidJbdcUtils.getConnetion(),
                sql,
                orderItem.getItemid(),
                orderItem.getCount(),
                orderItem.getSubtotal(),
                orderItem.getProduct().getPid(),
                orderItem.getOrder().getOid()) ;
        System.out.println(count);


    }


    /**
     * 带条件分页查询订单列表数据
     * @param currentPage 当前页码
     * @param pageSize  每页显示的条数
     * @param uid  用户id
     * @return  分页实体
     */
    @Override
    public List<Order> selectOrderByPage(int currentPage, int pageSize, String uid) throws SQLException, InvocationTargetException, IllegalAccessException {
        //创建执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        //带条件查询订单数据
        String sql = "select * from orders where uid = ? limit ?,?" ;
        List<Order> orderList   = qr.query(sql, new BeanListHandler<Order>(Order.class), uid, (currentPage - 1) * pageSize, pageSize);

       //遍历订单列表,封装数据
        for(Order order:orderList){
            System.out.println(order.getOid());
            //订单项和商品表之间有管理 :一个订单项中包含指定的商品
          sql = "SELECT * FROM orderitem oi ,product p WHERE oi.`pid` = p.`pid`  AND oid = ? " ;
          List<Map<String, Object>> list = qr.query(sql, new MapListHandler(), order.getOid());
          //遍历获取每一个实体
            for(Map<String,Object> map :list){
                //List:包含哪些实体(Map)  Product,OrderItem
                //订单项包含商品
                //封装Product
                Product product = new Product() ;
                BeanUtils.populate(product,map);
                //封装订单项
                OrderItem orderItem = new OrderItem() ;
                BeanUtils.populate(orderItem,map) ;
                //订单项封装商品
                orderItem.setProduct(product) ;
                //订单实体获取到了
                //订单里面很多订单项
                order.getItems().add(orderItem) ;

            }

        }

        return orderList;
    }


    /**
     * 查找订单的总记录数
     * @return
     */
    @Override
    public int selectTotalCount(String uid) throws SQLException {
        //创建执行对象
        QueryRunner qr  = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        //sql
        String sql = "select count(oid) from orders where uid = ?" ;
        Object obj = qr.query(sql, new ScalarHandler<>(),uid);
        String s = String.valueOf(obj);
        int count = Integer.parseInt(s);
        return count;
    }

    /**
     * 订单数据访问接口:通过订单编号获取订单
     * @param oid 订单编号
     * @return  返回订单实体
     */
    @Override
    public Order selectOrderByOid(String oid) throws SQLException {
        //执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        String sql = "select * from orders where oid = ?" ;

        Order order = qr.query(sql, new BeanHandler<Order>(Order.class), oid);
        return order;
    }

    @Override
    public int updateOrder(Order order) throws SQLException {
        //执行对象
        QueryRunner qr = new QueryRunner(DruidJbdcUtils.getDataSource()) ;
        String sql = "insert into orders(address,name,telephone) values(?,?,?)" ;


        int count = qr.update(sql,order.getAddress(),order.getName(),order.getTelephone());

        return count ;
    }
}

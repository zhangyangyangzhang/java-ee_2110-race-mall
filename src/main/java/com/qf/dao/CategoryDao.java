package com.qf.dao;

import com.qf.pojo.Category;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/24
 * 分类的数据访问接口
 */
public interface CategoryDao {
    /**
     * 分类数据访问接口获取所有分类信息
     * @return 分类的列表
     */
    List<Category> selectAllCategory() throws SQLException;
}

package com.qf.dao;

import com.qf.pojo.User;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/18
 * 针对用户访问数据接口
 */
public interface UserDao {
    /**
     * 数据接口查询所有的用户信息,封装List集合
     * @return 返回list列表
     */
    List<User> selectAllUser() throws SQLException;

    User selectUserById(String uid) throws SQLException;

    /**
     * 更新用户数据
     * @param user 用户实体
     * @return 影响行数
     */
    int update(User user) throws SQLException;

    /**
     * 用户数据接口删除用户通过uid
     * @param uid 用户uid
     * @return 影响行数
     */
    public int  deleteUserByUid(String uid) throws SQLException;

    /**
     * 通过分页查询用户列表数据
     * @param currentPage 当前页码
     * @param pageSize  每页显示条数
     * @return  返回用户列表数据
     */
    List<User> selectUserByPage(int currentPage, int pageSize) throws SQLException;

    /**
     * 查询用户的总记录数
     * @return 返回总计数
     */
    int getTotalCount() throws SQLException;

    /**
     * 用户的数据接口来通过用户查询用户
     * @param username  指定的用户名
     * @return 返回指定的用户实体
     */
    User selectUserByUsername(String username) throws SQLException;

    /**
     * 用户数据接口添加用户信息
     * @param user  用户实体类
     * @return 影响的行数
     */
    int addUser(User user) throws SQLException;

    /**
     * 用户数据接口:通过激活码 找用户
     * @param code 用户激活码
     * @return  返回用户实体
     */
    User selectUserByCode(String code) throws SQLException;

    /**
     * 将用户的状态更新
     * @param user
     */
    void updateUser(User user) throws SQLException;

    /**
     * 用户数据接口:通过用户名和密码查询用户
     * @param username
     * @param password
     * @return
     */
    User selectUser(String username, String password) throws SQLException;
}

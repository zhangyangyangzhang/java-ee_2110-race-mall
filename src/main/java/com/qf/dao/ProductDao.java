package com.qf.dao;

import com.qf.pojo.Product;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/25
 * 商品的数据访问接口
 */
public interface ProductDao {
    /**
     * 查询最新商品
     * @return 返回商品列表,限制9条记录
     */
    List<Product> selectNewProduct() throws SQLException;

    /**
     * 查询热门商品
     * @return 返回商品列表,限制9条记录
     */
    List<Product> selectHotProduct() throws SQLException;

    /**
     * 数据访问接口:通过商品编号查询商品
     * @param pid 商品编号
     * @return 返回商品实体
     */
    Product selectProductByPid(String pid) throws SQLException;

    /**
     * 获取商品的分类的列表数据
     * @param currentPage 当前页码
     * @param pageSize  每页显示的条数
     * @param cid  分类编号
     * @return  分页列表数据
     */
    List<Product> selectProductByPage(int currentPage, int pageSize, String cid) throws SQLException;

    /**
     * 通过分类编号查询总记录数
     * @param cid 分类编号
     * @return 返回总记录数
     */
    int selectTotalCount(String cid) throws SQLException;
}

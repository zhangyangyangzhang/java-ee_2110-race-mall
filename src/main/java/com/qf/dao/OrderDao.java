package com.qf.dao;

import com.qf.pojo.Order;
import com.qf.pojo.OrderItem;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/26
 * 订单数据访问接口
 */
public interface OrderDao {
    /**
     * 插入订单数据
     * @param order 订单实体
     */
    void insertOrder(Order order) throws SQLException;

     /**
      * 插入订单项数据
     * @param orderItem 订单实体
     */
    void insertOrderItem(OrderItem orderItem) throws SQLException;

    /**
     * 带条件分页查询订单列表数据
     * @param currentPage 当前页码
     * @param pageSize  每页显示的条数
     * @param uid  用户id
     * @return  分页实体
     */
    List<Order> selectOrderByPage(int currentPage, int pageSize, String uid) throws SQLException, InvocationTargetException, IllegalAccessException;

    /**
     * 查找订单的总记录数
     * @return
     */
    int selectTotalCount(String uid) throws SQLException;

    /**
     * 订单数据访问接口:通过订单编号获取订单
     * @param oid 订单编号
     * @return  返回订单实体
     */
    Order selectOrderByOid(String oid) throws SQLException;

    int updateOrder(Order order) throws SQLException;
}

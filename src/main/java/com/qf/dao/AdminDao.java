package com.qf.dao;

import com.qf.pojo.Admin;

import java.sql.SQLException;

/**
 * @author Kuke
 * @date 2021/11/17
 * 针对管理的数据访问接口
 */
public interface AdminDao {

    /**
     * 通过管理员名称获取实体
     * @param name  管理员名称
     * @return 返回管理实体
     */
    Admin selectAdminByName(String name) throws SQLException;
}

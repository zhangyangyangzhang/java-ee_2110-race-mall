package com.qf.convert;

import org.apache.commons.beanutils.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Kuke
 * @date 2021/11/18 17:42
 * 自定义日期转换器
 * //自定义类实现org.apache.commons.beanutils.Converter接口
 */
public class MyConvert  implements Converter {
    //转换方法
    @Override
    public Object convert(Class type, Object source) { //source进行解析

        try {
            //创建SimpledDateFormart
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd") ;
            //解析
            Date date = sdf.parse((String) source);
            return  date ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}

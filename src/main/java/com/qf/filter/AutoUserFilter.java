package com.qf.filter;

import com.qf.pojo.User;
import com.qf.service.UserService;
import com.qf.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/*
*   处理自动登录业务的过滤器
*   过滤的路径 /jsp/login.jsp
* 过滤的规则:
*       当前直击 在地址栏上访问/jsp/login.jsp 过滤或者是请求转发到jsp/login.jsp被过滤
* */
@WebFilter(urlPatterns = "/jsp/login.jsp",dispatcherTypes = {DispatcherType.REQUEST,DispatcherType.FORWARD})
public class AutoUserFilter implements Filter {

    //初始化
    public void init(FilterConfig config) throws ServletException {
        System.out.println("自动登录过滤器初始化了");
    }

    /**
     * 过滤任务
     * @param req  请求对象 ServletRequest
     * @param resp  响应对象 ServletRequest
     * @param chain 过滤链
     * @throws ServletException
     * @throws IOException
     */
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
       //将req----转换成HttpServletRequest 子接口
        HttpServletRequest request = (HttpServletRequest) req;
        //resp----转换成HttpServletResponse 子接口
        HttpServletResponse response = (HttpServletResponse) resp;

        //1)浏览器再次请求服务器---如果地址/jsp/login.jsp,过滤
        //获取cookie
        Cookie[] cookies = request.getCookies();
        if(cookies!=null){
            //有cookie存在
            //获取cookie内容
            //声明cookie的内容
            String content = null ;
            for(Cookie cookie : cookies){
               /* String name = cookie.getName();
                if(name.equals("autouser")){

                }*/
                    if(cookie.getName().equals("autouser")){
                        content = cookie.getValue() ;
                    }
            }
            //获取cookie的内容
            //如果content为null,放行
            if(content !=null){
                //将内容获取出来,---加密的内容----解码 URLDecoder
                content = URLDecoder.decode(content, "utf-8");
                System.out.println(content);
                //使用字符串的拆分功能,获取用户名和密码
                String[] strs = content.split(":");
                String username = strs[0] ;
                String password = strs[1] ;

                //调用业务接口
                UserService userService = new UserServiceImpl() ;
                User user = userService.findUserByUsernameAndPwd(username, password);
                if(user!=null){
                    //存在
                    //将user对象存储到session中
                    request.getSession().setAttribute("user",user) ;
                    //重定向到首页
                    response.sendRedirect(request.getContextPath()+"/");
                }
            }else{
                //放行
                chain.doFilter(req,resp);
            }


        }else{
            //没有cookie,就放行
            chain.doFilter(req, resp);
        }




    }


    public void destroy() {
        System.out.println("自动登录过滤器销毁了了");
    }


}

package com.qf.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 注解方式代替xml配置方式
 *
 * @WebFilter
 *      哪些属性:
 *             String filterName():过滤器的名称默认当前类名
 *             String[] value() ; value = "/配置过滤的路径"
 *             String[] urlPatterns() ; urlPatterns={"/xx","/xxx"}
 *             一个过滤器指定一个过滤路径:
 *                  value如果值配置一个路径的 ,写法等价于urlPatterns="/xx"
 *             DispatcherType[] dispatcherTypes():定义过滤的规则
 *                  默认值就是DispatcherType.REQUEST:默认直接地址栏请求拦截
 *                  DispatcherType.FORWARD:请求转发到某个资源的时候被拦截
 *                   DispatcherType.INCLUDE:请求的时候地址栏包含指定的路径的时候被路径
 *
 *
 */

@WebFilter("/*")//请求所有路径的时候都需要解决中文乱码
public class CharacterFilter implements Filter {


    public void init(FilterConfig config) throws ServletException {
       // config.getInitParameter("参数名称")
        System.out.println("全局过滤器被初始化了...");
    }

    //过滤任务
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
      /*  System.out.println("全局过滤器的过滤任务执行了...");*/
        //第一次请求 admin/admin_login.jsp----post提交
        //req:ServletRequest 是HttServletRequest的父接口
        HttpServletRequest request = (HttpServletRequest) req;
        //resp: ServletResponse: 是HttpservletResponse父接口
        HttpServletResponse response = (HttpServletResponse) resp;
        //获取前台提交方式
        String method = request.getMethod();
        //如果method和"POST"
        if("POST".equals(method)){
            //解决post乱码
            request.setCharacterEncoding("utf-8");
        }


        //设置服务器响应的中文乱码
        response.setContentType("text/html;charset=utf-8") ;

        //放行
        chain.doFilter(req, resp);
    }


    public void destroy() {
        System.out.println("过滤器销毁了...");
    }

}

package com.qf.web;

import com.qf.service.UserService;
import com.qf.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 通过用户id删除用户 的后台入门
 */
@WebServlet("/deleteUser")
public class DeleteUserServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //获取uid参数
        String uid = request.getParameter("uid");
       // System.out.println(uid);

        //调用用户的业务接口
        UserService userService = new UserServiceImpl() ;
        userService.deleteUser(uid) ;

        //请求转发到/findAll后台上
        request.getRequestDispatcher("/findAllUser").forward(request,response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}

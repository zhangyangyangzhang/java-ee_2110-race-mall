package com.qf.web;

import com.qf.pojo.User;
import com.qf.service.UserService;
import com.qf.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 通过用户id查询用户的后台
 */
@WebServlet("/findUserByUid")/*xml 配置---<select-name>*/
public class FindUserServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //1)接收参数
        String uid = request.getParameter("uid");
        //System.out.println(uid);

        //2)调用UserService
        UserService userService = new UserServiceImpl() ;
        User user = userService.findUserByUid(uid) ;
        //System.out.println(user);

        //3)将user对象存储request域中
        request.setAttribute("user",user);

        //4)请求转发到updateUser.jsp中
        request.getRequestDispatcher("/admin/update_user.jsp").forward(request,response);


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);

    }
}

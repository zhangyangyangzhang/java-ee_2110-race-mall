package com.qf.web;

import com.qf.pojo.Cart;
import com.qf.pojo.CartItem;
import com.qf.pojo.Product;
import com.qf.pojo.User;
import com.qf.service.ProductService;
import com.qf.service.impl.ProduceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * 购物车的后台入口
 */
@WebServlet("/cart")
public class CartServlet extends BaseServlet {

    //获取购物车从session中
    public Cart getCart(HttpServletRequest request) {
        //从session域中取出,判断是否null
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        //如果当前cart 为null,表示session域中没有购物车
        if(cart == null){
            //创建新的购物车
            cart = new Cart() ;
            //将购物车存储session域中
            HttpSession session = request.getSession();
            System.out.println(session.getId());


            session.setAttribute("cart",cart);
        }
        return  cart ;
    }

    /**
     * 将购物车项添加到购物车中
     * @param request
     */
    public void addCartItem2Cart(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        //从session中获取名称为"user"的用户实体
        User user = (User) request.getSession().getAttribute("user");
        if(user==null){
            //没有登录过
            //请去转发jsp/login.jsp
            request.getRequestDispatcher("/jsp/login.jsp").forward(request,response);
            return;
        }


        //接收参数
        //pid
        String pid = request.getParameter("pid");
        //商品的数量count
        String  quality = request.getParameter("count");
        int count = Integer.parseInt(quality) ;

        //调用商品的业务接口 :通过商品pid获取商品实体
        ProductService productService = new ProduceServiceImpl() ;
        Product product = productService.findProduct(pid);
        //封装购物车项
        CartItem cartItem = new CartItem(product,count) ;
        System.out.println(cartItem);
        //获取购物车
        Cart cart = getCart(request);
        //调用它的功能:将购物车项添到购物车中
        cart.addCartItem2Cart(cartItem) ;



        //重定向到jsp/cart.jsp
        response.sendRedirect(request.getContextPath()+"/jsp/cart.jsp");

    }

    /**
     * 删除购物车项
     * @param request 请求对象
     * @param response 响应对象
     */
    public  void delCartItem(HttpServletRequest request,HttpServletResponse response) throws IOException {
         //获取参数pid
        String pid = request.getParameter("pid");
        //获取购物车实体
        Cart cart = getCart(request) ;
        //调用它的删除购物车项的功能
        cart.removeCarItemFromCart(pid) ;

        //重定向到/jsp/cart.jsp
        response.sendRedirect(request.getContextPath()+"/jsp/cart.jsp");
    }


    /**
     * 清空购物车的方法
     * @param request 请求
     * @param response 响应
     */
    public void clearCart(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //获取购物车
        Cart cart = getCart(request);

        //调用清空购物车的方法
        cart.clearCart();
        //将session存储的cart remove掉
        request.getSession().removeAttribute("cart");

        //重定向到cart.jsp
        response.sendRedirect(request.getContextPath()+"/jsp/cart.jsp");
    }





}

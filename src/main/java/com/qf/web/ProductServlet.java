package com.qf.web;

import com.qf.pojo.PageBean;
import com.qf.pojo.Product;
import com.qf.service.ProductService;
import com.qf.service.impl.ProduceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 商品模块的servlet入口fut
 */
@WebServlet("/product")
public class ProductServlet extends BaseServlet {

    public void findProduct(HttpServletRequest request,HttpServletResponse response) throws Exception {
        //接收商品的pid编号
        String pid = request.getParameter("pid");
        //System.out.println(pid);

        //调用商品的业务接口
        ProductService productService = new ProduceServiceImpl() ;
        Product product = productService.findProduct(pid) ;
        System.out.println(product);

        //将商品实体存储在request域中
        request.setAttribute("product",product) ;
        //请求转发到jsp/product_info.jsp
        request.getRequestDispatcher("/jsp/product_info.jsp").forward(request,response);
    }

    /**
     * 分页查询商品数据
     * @param request 请求对象
     * @param response 响应对象
     */
    public  void findProductByPage(HttpServletRequest request,HttpServletResponse response) throws Exception {

        //接收参数
        String curPage = request.getParameter("currentPage");
        String pSize = request.getParameter("pageSize");
        //转换成int类型
        //当前页码
        int currentPage = Integer.parseInt(curPage);
       // System.out.println(currentPage);
        //每页显示的条数
        int pageSize = Integer.parseInt(pSize) ;
       // System.out.println(pageSize);
        //获取分类编号
        String cid = request.getParameter("cid");
        //System.out.println(cid);

        //调用商品的业务接口
        ProductService productService = new ProduceServiceImpl() ;
        PageBean<Product> pb = productService.findProductByPage( currentPage, pageSize, cid) ;

        //将PageBean实体存储在request域中
        request.setAttribute("pb",pb) ;
        //请求转发到商品列表页面
        request.getRequestDispatcher("/jsp/product_list.jsp").forward(request,response);

    }

}

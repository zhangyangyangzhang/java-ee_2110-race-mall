package com.qf.web;

import com.qf.pojo.User;
import com.qf.service.UserService;
import com.qf.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 注册完后激活用户的servlet
 */
@WebServlet("/activeUser")
public class ActiveUserServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         // http://localhost:8080/maven_study_02_war/activeUser?code=FF174D187BB3444595B1D7AFA4DA9B8B
        //接收code这个参数
        String code = request.getParameter("code");
        //调用业务接口
        UserService userService = new UserServiceImpl() ;
        User user = userService.findUserByCode(code) ;

        //判断
        if(user!=null){
            //提示信息
            request.setAttribute("msg","用户激活成功,请<a href='/maven_study_02_war/jsp/login.jsp'>登录</a>");
            //请求转发msg.jsp
            request.getRequestDispatcher("/jsp/msg.jsp").forward(request,response) ;
        }else{
            //提示信息
            request.setAttribute("msg","用户激活失败,请重新激活!");
            //请求转发msg.jsp
            request.getRequestDispatcher("/jsp/msg.jsp").forward(request,response) ;
        }

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doGet(request,response);
    }
}

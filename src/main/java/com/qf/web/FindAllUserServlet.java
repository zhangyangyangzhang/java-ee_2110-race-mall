package com.qf.web;

import com.qf.pojo.User;
import com.qf.service.UserService;
import com.qf.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 这个是针对查询所有用户的后台
 */
@WebServlet("/findAllUser")
public class FindAllUserServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //调用UserService,获取业务数据
        UserService userService = new UserServiceImpl() ;
        List<User> list = userService.findAllUser() ;

        //判断
        if(list!=null){
            //将list集合存储request域中
            request.setAttribute("list",list);
            //请求转发到admin/userList.jsp
            request.getRequestDispatcher("/admin/userList.jsp").forward(request,response); ;
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doGet(request,response);
    }
}

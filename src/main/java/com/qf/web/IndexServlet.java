package com.qf.web;

import com.qf.pojo.Category;
import com.qf.pojo.Product;
import com.qf.service.CategoryService;
import com.qf.service.ProductService;
import com.qf.service.impl.CategoryServiceImpl;
import com.qf.service.impl.ProduceServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/index")
public class IndexServlet extends BaseServlet {

        public void index(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {


            //调用商品的业务接口
            ProductService productService = new ProduceServiceImpl() ;
            //获取最新商品
            List<Product> newList = productService.findNewProduct() ;
            //获取热门商品
            List<Product> hotList = productService.findHotProduct() ;

            //将newList和hotList存储在request域中
            request.setAttribute("newList",newList) ;
            request.setAttribute("hotList",hotList) ;
            System.out.println(newList) ;
            System.out.println(hotList) ;

            //请求转发到/jsp/index.jsp
            request.getRequestDispatcher("/jsp/index.jsp").forward(request,response);
        }

}

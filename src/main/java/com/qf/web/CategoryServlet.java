package com.qf.web;

import com.qf.pojo.Category;
import com.qf.service.CategoryService;
import com.qf.service.impl.CategoryServiceImpl;
import com.qf.utils.JsonUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 分类信息展示的后台入口
 */
@WebServlet("/category")
public class CategoryServlet extends BaseServlet {

    /**
     * 查询所有的分类信息
     * @param request 请求对象
     * @param response 响应对象
     */
    public void findAllCategory(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //System.out.println("已经进入到分类信息展示的后台入口");

        //调用分类业务接口查询所有分类
        CategoryService categoryService = new CategoryServiceImpl() ;
        List<Category> cList = categoryService.getAllCategory() ;

        //将clist转换成json格式
        String jsonStr = JsonUtils.list2json(cList);
        System.out.println(jsonStr);
        //服务器响应给浏览器
        response.getWriter().write(jsonStr);
    }

}

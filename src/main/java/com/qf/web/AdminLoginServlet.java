package com.qf.web;

import com.qf.pojo.Admin;
import com.qf.service.AdminService;
import com.qf.service.impl.AdminServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * web项目 版本3.0之后就支持注解
 * @WebServlet ----代替了 繁琐的xml配置方式
 *
 *  常用的 属性
 *          String[] value()
 *          String[] urlPatterns()  等价的 指定的映射路径 <url-pattern></url-pattern>
 *           可以配置多个映射路径,一个servlet配置一个路径
 *
 *           String name()---代替了 <servlet-name></servlet-name>
 *                      name属性:默认的就是当前类名
 *
 *           int loadOnStartup():代表之前servlet配置 <load-on-startup>1</load-on-startup> 这个类的创建时机
 */
//@WebServlet(urlPatterns ="/adminlogin",loadOnStartup = 1)
 @WebServlet("/adminlogin") //必须"/开头"
public class AdminLoginServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //登录:post提交,post提交,自己手动解决乱码
        //request.setCharacterEncoding("utf-8");

        //1)接收前台参数
        String name = request.getParameter("adminname");
        String password = request.getParameter("adminpassword");

        //2)调用业务层接口获取数据
        AdminService adminService = new AdminServiceImpl() ;
        Admin admin = adminService.findAdmin(name, password);

        if(admin!=null){
            //3)将admin实体存储在HttpSession中: 域对象(在一次会话中)
            //HttpSession和Cookie :最大的区别: 前者:服务器端存  后者:浏览器中
            //HttpSession ---可以通过request对象的方法 getSession(): 第一次空的,创建Session
            //public HttpSession getSession():返回与此请求关联的当前会话，如果该请求没有会话，则创建一个会话。
            HttpSession session = request.getSession();//第一次，创建Session会话
            session.setAttribute("admin",admin);

            //请求转发到主页中,\
            request.getRequestDispatcher("/admin/admin_index.jsp").forward(request,response);
        }else{

            //提示错误数据
            //request域设置提示信息数据
            request.setAttribute("msg","用户名或者密码错误!");
            //请求转发到登录页面提示
            request.getRequestDispatcher("/admin/admin_login.jsp").forward(request,response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doGet(request,response);
    }
}

package com.qf.web;

import com.qf.pojo.PageBean;
import com.qf.pojo.User;
import com.qf.service.UserService;
import com.qf.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 分页查询用户的后台入口
 */
@WebServlet("/findUserByPage")
public class FindUserByPageServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //接收参数并进行判断,给定默认值
        //currentPage /pageSize两个参数
        String curPage = request.getParameter("currentPage");
        String pSize = request.getParameter("pageSize");
        if(curPage== null || curPage.equals("")){
            //默认值
            curPage= "1" ;//第一页
        }
        if(pSize== null || pSize.equals("")){
            pSize = "2" ; //每页显示2条
        }

        //类型转换
        int currentPage = Integer.parseInt(curPage) ;
        int pageSize = Integer.parseInt(pSize) ;

        //调用用户业务接口
        UserService userService = new UserServiceImpl() ;
        PageBean<User> pb = userService.findUserByPage(currentPage,pageSize) ;
        System.out.println(pb);
        if(pb!=null){
            //存储在request域中
            request.setAttribute("pb",pb) ;
            //请求转发到userList.jsp
            request.getRequestDispatcher("/admin/userList.jsp").forward(request,response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request,response);
    }
}

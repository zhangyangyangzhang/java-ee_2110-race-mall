package com.qf.utils;


import com.qf.dao.CategoryDao;
import com.qf.dao.impl.CategoryDaoImpl;
import com.qf.pojo.Category;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;
import net.sf.json.xml.XMLSerializer;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @author Kuke
 * @date 2021/9/1
 *
 * JsonArray:将数组或者list集合---->Json对象的解析器
 * JsonObject:将Map集合或者实体JavaBean---->Json对象的解析器
 
 jsonlib.jar包
 
		解析器:JsonArray:将List集合或者数组----json字符串
		        JsonObject :将Map集合或者实体对象--json字符串
 */
public class JsonUtils {
    /**
     * 将数组转换成String类型的JSON数据格式
     *
     * @param objects
     * @return

    {"key":value}
     */
    public static String array2json(Object[] objects){

        JSONArray jsonArray = JSONArray.fromObject(objects);
        return jsonArray.toString();

    }

    /**
     * 将list集合转换成String类型的JSON数据格式
     *
     * @param list
     * @return
     */
    public static String list2json(List list){

        JSONArray jsonArray = JSONArray.fromObject(list);
        return jsonArray.toString();

    }

    /**
     * 将map集合转换成String类型的JSON数据格式
     *
     * @param map
     * @return
     */
    public static String map2json(Map map){

        JSONObject jsonObject = JSONObject.fromObject(map);
        return jsonObject.toString();

    }

    /**
     * 将Object对象转换成String类型的JSON数据格式
     *
     * @param object
     * @return
     */
    public static String object2json(Object object){

        JSONObject jsonObject = JSONObject.fromObject(object);
        return jsonObject.toString();

    }

    /**
     * 将XML数据格式转换成String类型的JSON数据格式
     *
     * @param xml
     * @return
            <username>hello</username>

    "username":"hello"
     */
    public static String xml2json(String xml){

        JSONArray jsonArray = (JSONArray) new XMLSerializer().read(xml);
        return jsonArray.toString();

    }

    /**
     * 除去不想生成的字段（特别适合去掉级联的对象）
     *
     * @param excludes
     * @return
     */
    public static JsonConfig configJson(String[] excludes) {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setExcludes(excludes);
        jsonConfig.setIgnoreDefaultExcludes(true);
        jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        return jsonConfig;
    }

    public static void main(String[] args) throws SQLException {

        //创建分类的数据接口对象
        CategoryDao categoryDao = new CategoryDaoImpl() ;
        List<Category> list = categoryDao.selectAllCategory();
        System.out.println(list);
        String jsonStr = JsonUtils.list2json(list);
        System.out.println(jsonStr);


    }

}

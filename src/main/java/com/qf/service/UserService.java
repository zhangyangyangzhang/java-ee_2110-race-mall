package com.qf.service;

import com.qf.pojo.PageBean;
import com.qf.pojo.User;

import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/18
 * 针对用户的业务接口
 */
public interface UserService {
    /**
     * 获取所有用户
     * @return 返回用户列表
     */
    List<User> findAllUser();

    /**
     * 通过用户的uid查询用户
     * @param uid 用户uid
     * @return  返回用户
     */
    User findUserByUid(String uid);

    /**
     * 更新用户
     * @param user 用户实体
     */
    void updateUser(User user);

    /**
     * 通过用户uid删除用户
     * @param uid 用户uid编号
     */
    void deleteUser(String uid);

    /**
     * 分页查询用户数据
     * @param currentPage 当前页码,默认第一页
     * @param pageSize  每页显示的 条数,默认2条
     * @return 分页实体
     */
    PageBean<User> findUserByPage(int currentPage, int pageSize);

    /**
     * 前台注册时校验用户名
     * @param username 用户名
     * @return 返回true,能注册;否则,不能注册!
     */
    boolean checkUser(String username);

    /**
     * 注册用户
     * @param user 用户实体
     */
    void registerUser(User user);

    /**
     * 通过用户的激活码找用户
     * @param code  激活码
     * @return 返回的用户实体
     */
    User findUserByCode(String code);

    /**
     * 通过用户名和密码寻找用户的业务层方法
     * @param username 用户名
     * @param password 密码
     * @return  返回用户实体
     */
    User findUserByUsernameAndPwd(String username, String password);
}

package com.qf.service;

import com.qf.pojo.Order;
import com.qf.pojo.PageBean;

/**
 * @author Kuke
 * @date 2021/11/26
 * 订单业务接口
 */
public interface OrderService {
    /**
     * 生成订单
     * @param order 订单实体
     */
    void add(Order order);

    /**
     * 订单列表分页
     * @param currentPage 当前页码
     * @param pageSize  每页显示的条数
     * @param uid  用户uid
     * @return 分页实体
     */
    PageBean<Order> getOderByPage(int currentPage, int pageSize, String uid);

    /**
     * 通过订单编号获取订单
     * @param oid  订单编号
     * @return  返回订单实体
     */
    Order getOrderById(String oid);

    //更新订单的地址/收货人/电话
    void update(Order order);
}

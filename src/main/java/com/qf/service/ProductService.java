package com.qf.service;

import com.qf.pojo.PageBean;
import com.qf.pojo.Product;

import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/25
 * 针对商品的业务接口
 */
public interface ProductService {
    /**
     * 获取最新商品
     * @return 返回商品列表
     */
    List<Product> findNewProduct();

    /**
     * 获取热门商品
     * @return 返回商品列表
     */
    List<Product> findHotProduct();

    /**
     * 通过商品的编号获取商品实体
     * @param pid  商品的编号
     * @return 返回商品实体
     */
    Product findProduct(String pid);

    /**
     * 分页获取商品的数据
     * @param currentPage 当前页码 默认1
     * @param pageSize  每页显示的条数 12
     * @param cid 当前分类编号
     * @return  分页的实体
     */
    PageBean<Product> findProductByPage(int currentPage, int pageSize, String cid);
}

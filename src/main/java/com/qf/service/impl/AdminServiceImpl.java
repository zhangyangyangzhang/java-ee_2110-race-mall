package com.qf.service.impl;

import com.qf.dao.AdminDao;
import com.qf.dao.impl.AdminDaoImpl;
import com.qf.pojo.Admin;
import com.qf.service.AdminService;

import java.sql.SQLException;

/**
 * @author Kuke
 * @date 2021/11/17 11:47
 *针对管理员的业务接口实现
 */
public class AdminServiceImpl implements AdminService {
    @Override
    public Admin findAdmin(String name, String password) {

        try {
            //调用数据访问接口
            AdminDao adminDao = new AdminDaoImpl() ;
            Admin admin = adminDao.selectAdminByName(name);

            if(admin!=null){
                //业务逻辑判断
                //获取当前管理员的密码和前台输入的password进行对比
                if(admin.getAdminpassword().equals(password)){
                    //信息一致
                    return  admin ;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

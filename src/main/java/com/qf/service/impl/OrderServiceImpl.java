package com.qf.service.impl;

import com.qf.dao.OrderDao;
import com.qf.dao.impl.OrderDaoImpl;
import com.qf.pojo.Order;
import com.qf.pojo.OrderItem;
import com.qf.pojo.PageBean;
import com.qf.service.OrderService;
import com.qf.utils.DruidJbdcUtils;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/26 17:23
 * 订单业务接口实现
 */
public class OrderServiceImpl implements OrderService {
    @Override
    public void add(Order order) {
        Connection connetion = null ;
        try {
            //使用事务管理
           // DruidJbdcUtils.startStransaction();  //开启事务
            connetion  = DruidJbdcUtils.getConnetion();
            connetion.setAutoCommit(false) ;
            //调用OrderDao订单的数据访问接口
            OrderDao orderDao = new OrderDaoImpl();

            //给订单表中插入数据
            orderDao.insertOrder(order) ;


            //异常出现了
            //int i = 10 / 0 ;

            //获取每一个订单项
            for(OrderItem orderItem : order.getItems()){
                //调用数据访问接口给订单项也插入数据
                orderDao.insertOrderItem(orderItem) ;
            }
            //手动提交事务
           // DruidJbdcUtils.commitAndClose();
            connetion.commit();
        } catch (SQLException e) {

            try {
                connetion.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            //调用回滚动作
//            try {
//                DruidJbdcUtils.rollBackAndClose();
//            } catch (SQLException ex) {
//                ex.printStackTrace();
//            }
//            //e.printStackTrace();
        }

    }


    /**
     * 订单列表分页
     * @param currentPage 当前页码
     * @param pageSize  每页显示的条数
     * @param uid  用户uid
     * @return 分页实体
     */
    @Override
    public PageBean<Order> getOderByPage(int currentPage, int pageSize, String uid) {
        //调用的数据访问接口OrderDao
        try {
            OrderDao orderDao = new OrderDaoImpl() ;
            //带条件分页查询订单列表数据
            List<Order> list = orderDao.selectOrderByPage(currentPage,pageSize,uid) ;

            //查询当前订单的总记录数
            int totalCount = orderDao.selectTotalCount(uid) ;


            return new PageBean<>(currentPage,pageSize,totalCount,list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null ;
    }
    /**
     * 通过订单编号获取订单
     * @param oid  订单编号
     * @return  返回订单实体
     */
    @Override
    public Order getOrderById(String oid) {
        //调用订单数据访问接口
        try {
            OrderDao orderDao = new OrderDaoImpl() ;
            Order order = orderDao.selectOrderByOid(oid) ;
            if(order!=null){
                return  order ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Order order) {
        try {
            OrderDao orderDao = new OrderDaoImpl() ;
            int count = orderDao.updateOrder(order) ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

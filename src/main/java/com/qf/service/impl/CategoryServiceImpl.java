package com.qf.service.impl;

import com.qf.dao.CategoryDao;
import com.qf.dao.impl.CategoryDaoImpl;
import com.qf.pojo.Category;
import com.qf.service.CategoryService;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/24 14:35
 * 分类的业务接口实现
 */
public class CategoryServiceImpl implements CategoryService {
    /**
     * 获取所有的分类
     * @return 返回分类列表
     */
    @Override
    public List<Category> getAllCategory() {
        try {
            //调用分类的数据访问接口
            CategoryDao categoryDao = new CategoryDaoImpl() ;
            List<Category> list = categoryDao.selectAllCategory() ;
            if(list!=null){
                return  list ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package com.qf.service.impl;

import com.qf.dao.UserDao;
import com.qf.dao.impl.UserDaoImpl;
import com.qf.pojo.PageBean;
import com.qf.pojo.User;
import com.qf.service.UserService;
import com.qf.utils.MailUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/18 14:29
 * 针对用户的业务接口实现
 */
public class UserServiceImpl implements UserService {
    /**
     * 获取所有用户
     * @return 返回用户列表
     */
    @Override
    public List<User> findAllUser() {
        try {
            //调用用户的数据接口
            UserDao userDao = new UserDaoImpl() ;
            List<User> userList = userDao.selectAllUser() ;
            if(userList!=null){
                return  userList ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User findUserByUid(String uid) {
        try {
            UserDao userDao = new UserDaoImpl() ;//调用数据接口
            User user = userDao.selectUserById(uid);
            if(user!=null){
                return  user ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 更新用户
     * @param user 用户实体
     */
    @Override
    public void updateUser(User user) {

        //调用数据接口
        try {
            UserDao userDao = new UserDaoImpl() ;
            int count = userDao.update(user) ; //影响行数
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过用户uid删除用户
     * @param uid 用户uid编号
     */
    @Override
    public void deleteUser(String uid) {

        //调用用户数据访问接口
        try {
            UserDao userDao = new UserDaoImpl() ;
            int i = userDao.deleteUserByUid(uid);
            System.out.println(i);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 分页查询用户数据
     * @param currentPage 当前页码,默认第一页
     * @param pageSize  每页显示的 条数,默认2条
     * @return 分页实体
     */
    @Override
    public PageBean<User> findUserByPage(int currentPage, int pageSize) {
        //调用用户的数据访问接口
        try {
            UserDao userDao = new UserDaoImpl() ;
            //分页查询用户列表数据
            List<User> list = userDao.selectUserByPage(currentPage,pageSize) ;
            //查询当前用户的总记录数
            int totalCount = userDao.getTotalCount() ;

            //封装PageBean实体
            return new PageBean<>(currentPage,pageSize,totalCount,list);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  null ;
    }

    /**
     * 前台注册时校验用户名
     * @param username 用户名
     * @return 返回true,能注册;否则,不能注册!
     */
    @Override
    public boolean checkUser(String username) {
        try {
            //调用业务接口
            UserDao userDao = new UserDaoImpl() ;
            User user = userDao.selectUserByUsername(username) ;
            if(user!=null){
                //已经存了,不能注册
                return  true ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;//用户不存在,可以注册
    }

    /**
     * 注册用户
     * @param user 用户实体
     */
    @Override
    public void registerUser(User user) {

        //调用数据访问接口
        try {
            UserDao userDao = new UserDaoImpl() ;
            int count = userDao.addUser(user) ;//调用数据接口给数据库添加用户数据

            //等会用户激活...
            String content = "欢迎注册,成员会员,请<a href='http://localhost:8080/maven_study_02_war/user?methodName=active&code="+user.getCode()+"'>点击激活</a>" ;
            MailUtils.sendMail(user.getEmail(),content,"用户的邮件激活") ;

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过用户的激活码找用户
     * @param code  激活码
     * @return 返回的用户实体
     */
    @Override
    public User findUserByCode(String code) {
        try {
            //调用数据接口
            UserDao userDao = new UserDaoImpl() ;
            User user = userDao.selectUserByCode(code) ;
            //user==null
            if(user==null){
                return  null ;
            }

            //调用数据接口,将用户的激活码从0变成1
            user.setState(1);
            userDao.updateUser(user) ;
            return user ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过用户名和密码寻找用户的业务层方法
     * @param username 用户名
     * @param password 密码
     * @return  返回用户实体
     */
    @Override
    public User findUserByUsernameAndPwd(String username, String password) {
        //调用数据访问接口
        try {
            UserDao userDao  = new UserDaoImpl() ;
            User user = userDao.selectUser(username,password) ;
            if(user!=null){
                return  user ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

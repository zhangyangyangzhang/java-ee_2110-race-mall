package com.qf.service.impl;

import com.qf.dao.ProductDao;
import com.qf.dao.impl.ProductDaoImpl;
import com.qf.pojo.PageBean;
import com.qf.pojo.Product;
import com.qf.service.ProductService;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/25 10:39
 * 商品的业务接口实现
 */
public class ProduceServiceImpl implements ProductService {
    /**
     * 获取最新商品
     * @return 返回商品列表
     */
    @Override
    public List<Product> findNewProduct() {
        //调用商品的数据访问接口ProductDao
        try {
            ProductDao productDao = new ProductDaoImpl() ;
            List<Product> newList = productDao.selectNewProduct() ;
            if(newList!=null){
                return newList ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取热门商品
     * @return 返回商品列表
     */
    @Override
    public List<Product> findHotProduct() {
        //调用商品的数据访问接口ProductDao
        try {
            ProductDao productDao = new ProductDaoImpl() ;
            List<Product> hotList = productDao.selectHotProduct() ;
            if(hotList!=null){
                return hotList ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过商品的编号获取商品实体
     * @param pid  商品的编号
     * @return 返回商品实体
     */
    @Override
    public Product findProduct(String pid) {
        //调用商品的数据访问接口
        try {
            ProductDao productDao = new ProductDaoImpl() ;
            Product product = productDao.selectProductByPid(pid) ;
            if(product!=null){

                return product;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  null ;
    }

    /**
     * 分页获取商品的数据
     * @param currentPage 当前页码 默认1
     * @param pageSize  每页显示的条数 12
     * @param cid 当前分类编号
     * @return  分页的实体
     */
    @Override
    public PageBean<Product> findProductByPage(int currentPage, int pageSize, String cid) {
        try {
            //调用商品的数据访问接口
            ProductDao productDao = new ProductDaoImpl() ;
            //获取分页的商品列表数据
            List<Product> list = productDao.selectProductByPage(currentPage,pageSize,cid) ;
            //获取总记录数
            int totalCount = productDao.selectTotalCount(cid) ;
            if(list!=null && totalCount !=0){
                return new PageBean<>(currentPage,pageSize,totalCount,list);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  null ;
    }
}

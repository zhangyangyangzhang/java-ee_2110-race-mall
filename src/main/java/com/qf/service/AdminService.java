package com.qf.service;

import com.qf.pojo.Admin;

/**
 * @author Kuke
 * @date 2021/11/17
 * 针对管理的业务接口
 */
public interface AdminService {
    /**
     * 通过管理员名称和密码获取数据
     * @param name  管理员名称
     * @param password 密码
     * @return 返回管理员实体
     */
    Admin findAdmin(String name, String password) ;
}

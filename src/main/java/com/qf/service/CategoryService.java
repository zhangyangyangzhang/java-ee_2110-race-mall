package com.qf.service;

import com.qf.pojo.Category;

import java.util.List;

/**
 * @author Kuke
 * @date 2021/11/24
 * 针对分类的业务接口
 */
public interface CategoryService {

    /**
     * 获取所有的分类
     * @return 返回分类列表
     */
    List<Category> getAllCategory();
}

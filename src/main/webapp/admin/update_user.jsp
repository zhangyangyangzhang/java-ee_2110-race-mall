<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/18
  Time: 16:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--引入格式化库--%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改页面</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <script  src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="width: 400px;">
    <h3 style="text-align: center;">修改用户信息</h3>

    <%--表单提交--%>
    <form class="form-horizontal" action="${pageContext.request.contextPath}/updateUser" method="post">

        <%--隐藏域,没有任何效果,但可以传递参数--%>
        <input type="hidden"  name="uid" value="${user.uid}" />


        <div class="form-group">
            <label for="username" class="col-sm-2 control-label">昵称</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="username"  value="${user.username}"  id="username" >
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">密码</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" name="password" value="${user.password}" id="password" >
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">姓名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name"  value= "${user.name}"  id="name" >
            </div>
        </div>
        <div class="form-group">
            <label for="sex" class="col-sm-2 control-label">性别</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="sex" value="${user.sex}" id="sex" >
            </div>
        </div>

        <div class="form-group">
            <label for="birthday" class="col-sm-2 control-label">出生日期</label>
            <div class="col-sm-10">


                <%--readonly:只读格式--%>
                <input type="date" readonly="readonly" name="birthday" id="birthday" class="form-control"
                       <%--
                                fmt:formatDate
                        value="<fmt:formatDate value="" pattern="yyyy-MM-dd"/>
                       --%>
                       value="<fmt:formatDate value="${user.birthday}" pattern="yyyy-MM-dd"/>" id="birthday" >
            </div>
        </div>
        <%--<div class="form-group">
            <label for="state" class="col-sm-2 control-label">状态</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="state"   id="state" >
            </div>
        </div>--%>

        <div class="form-group" style="text-align: center">
            <input class="btn btn-primary" type="submit" value="修改" />
            <input class="btn btn-default" type="reset" value="重置" />
            <input class="btn btn-default" type="button" value="返回"/>
        </div>

    </form>
</div>

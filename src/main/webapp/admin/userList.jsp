<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/18
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--导入核心标签库--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户列表</title>
    <!--先导入boostrap.min.css-->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet"/>
    <!--jquery-->
    <script src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js"></script>
    <!--boostrap的js库-->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
<%--
    table-responsive：自动响应式表格
--%>
<div class="table-responsive">


    <table class="table table-hover table-condensed table-bordered">
        <tr>
            <th>编号</th>
            <th>网名</th>
            <th>密码</th>
            <th>姓名</th>

            <th>电话</th>
            <th>生日</th>
            <th>性别</th>
            <th>操作</th>

        </tr>
        <%--之前的查询所有--%>
        <%--<c:forEach items="${list}" var="u">--%>
        <%--分页查询--%>
        <c:forEach items="${pb.list}" var="u">
            <tr>
                <%--这里每一个u就是实体对象
                        getXxx()  get() 去掉  Xxx第一个字母小写  xxx就是当前bean属性
                --%>
                <td>${u.uid}</td>
                <td>${u.username}</td>
                <td>${u.password}</td>
                <td>${u.name}</td>

                <td>${u.telephone}</td>
                <td>${u.birthday}</td>
                <td>${u.sex}</td>
                 <td>
                     <a href="${pageContext.request.contextPath}/findUserByUid?uid=${u.uid}" class="btn btn-primary">修改</a>
                    <%-- <a href="javascript:void(0)" onclick="testUpdate()" class="btn btn-primary">修改</a>--%>
                     <a href="javascript:void(0);" onclick="deleteItem('${u.uid}')" class="btn btn-danger">删除</a>
                 </td>
               <%-- <td>${u.state}</td>
                <td>${u.code}</td>--%>
            </tr>
        </c:forEach>
    </table>
    <div style="float: right;margin-right: 30px">
        <span>
            共有${pb.totalCount}条记录
        </span>
        <span>
            共有${pb.totalPage}页
        </span>
    </div>
</div>
<div style="margin: 0 0 0 420px">
    <nav aria-label="Page navigation">
        <ul class="pagination">


            <%--
                    判断为第一页
            --%>
            <c:if test="${pb.currentPage==1}">
                <%--
                    上一页翻页不了,禁用状态
                --%>
                <li class="disabled">
                    <a href="javascript:void(0);" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            </c:if>

                <%--
                    判断如果不是第一页
                --%>

                <c:if test="${pb.currentPage!=1}">

                    <li>
                        <a href="${pageContext.request.contextPath}/findUserByPage?currentPage=${pb.currentPage-1}&pageSize=2" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                </c:if>


            <%--遍历当前页码--%>
                <c:forEach begin="1" end="${pb.totalPage}" var="n">

                    <%--判断是否为当前页--%>
                    <c:if test="${pb.currentPage==n}">
                        <%--class="active"-激活状态 --%>
                        <li class="active" >
                            <a href="#">${n}</a>
                        </li>
                    </c:if>
                    <%--
                        如果不是当前页码
                    --%>
                    <c:if test="${pb.currentPage!=n}">
                        <li >
                            <a href="${pageContext.request.contextPath}/findUserByPage?currentPage=${n}&pageSize=2">${n}</a>
                        </li>
                    </c:if>




                </c:forEach>
                <%--
                    判断是否为最后一页

                    如果最后一页
                --%>
                <c:if test="${pb.currentPage==pb.totalPage}">
                    <%--禁用状态--%>
                    <li class="disabled">
                        <a href="javascript:void(0);" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </c:if>

                <%--如果不是最后一页--%>
                <c:if test="${pb.currentPage!=pb.totalPage}">

                    <li>
                        <a href="${pageContext.request.contextPath}/findUserByPage?currentPage=${pb.currentPage+1}&pageSize=2" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </c:if>

        </ul>
    </nav>

</div>
</body>
<script>

    //超链接删除(让超链接失效了)--->触发点击事件
   function deleteItem(uid) {
       //测试
       //alert(uid) ;

       //友情提示:确认是否删除? 弹出确认提示框
      var flag  = window.confirm("您确认是否删除吗?");

      if(flag){
            //删除
          //提交后台---window对象location对象.href属性
          window.location.href="${pageContext.request.contextPath}/deleteUser?uid="+uid ;
      }


   }
</script>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/18
  Time: 11:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>menu</title>
    <!--先导入boostrap.min.css-->
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet"/>
    <!--jquery-->
    <script src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js"></script>
    <!--boostrap的js库-->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
<!-- Single button -->
<div class="btn-group">
    <button style="padding-left: 40px"
            type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        用户管理
            <%--后面的箭头--%>
        <span class="caret"></span>
    </button>
    <!---->
    <ul class="dropdown-menu">
        <%--
            前台用户注册的时候,将信息添加后台
        --%>
        <%--<li><a href="#">添加用户</a></li>--%>

            <%--
                超链接标签 target属性:打开资源文件方式
                           1) _blank 新建窗口
                            _self:当前窗口直接点开


                           2)在指定的哪一个框架标签中打开资源连接
                             target属性需要和框架标签的name要一致
            --%>
        <%--<li><a href="${pageContext.request.contextPath}/findAllUser" target="main">查询所有用户</a></li>--%>
        <li><a href="${pageContext.request.contextPath}/findUserByPage" target="main">查询所有用户</a></li>

    </ul>
</div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/18
  Time: 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--导入核心标签库--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>logo</title>
</head>
<body>
    <%--<h3>logo部分</h3>--%>
    <span style="float: right;margin: 0 0 20px 0  ">
        <c:if test="${empty admin}">
            <a href="#">登录</a>
        </c:if>
        <%--从session域对象中绑定的属性admin 内容不为空,显示管理员登录了--%>
        <c:if test="${not empty admin}">
            ${admin.adminname},<a href="#">[退出]</a>
        </c:if>
    </span>
</body>
</html>

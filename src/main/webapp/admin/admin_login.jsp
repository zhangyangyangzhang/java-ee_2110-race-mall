<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/17
  Time: 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理登录</title>
    <style>

        #main{
            width: 400px;
            height: 200px;
            border: 1px solid #000;
            margin: 180px 0 0 400px;
        }

        #user{
            margin: 30px 0 0 100px;
        }
        #pwd{
            margin: 20px 0 0 100px;
        }
        #btn{
            margin: 20px 0 0 160px;
        }
        #tip{
            font-size: 15px;
            color: red;
            margin: 20px 0 0 60px;
        }
    </style>

    <%--导入boostrap的css样式--%>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
    <%--导入Jquery的js库--%>
    <script src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js"></script>
    <%--导入boostrap的核心js库--%>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>
<body>
    <div id="main">
        <form action="${pageContext.request.contextPath}/adminlogin" method="post">
            <font id="tip">${msg}</font>
            <div id="user">
                用户名:<input  type="text" placeholder="请输入用户名" name="adminname" />
            </div>
            <div id="pwd">
                密&ensp;&ensp;码:<input type="password" placeholder="请输入密码" name="adminpassword" />
            </div>
            <div id="btn">
                <%--boostrap按钮样式--%>
                <input type="submit" class="btn btn-primary" value="普通账号登录" />
                <%--<input type="button" value="二维码扫一扫"/>--%>
            </div>
        </form>
    </div>
</body>
</html>

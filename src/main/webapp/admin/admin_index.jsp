<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/17
  Time: 12:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>后端管理系统</title>
</head>

<%--框架标签frame 一个frame包含一个html
        整个框架如果有很多页面组成---框架集 frameset

    rows: 由上而下---纵向划分为几个部分,每一个部分所占的权重百分比
            每一个部分就是frame----一个frame包含一个html页面
    cosl:从左而右---横向划分几个部分,每一个部分所占的权重百分比

            *表示:出去上面和下面部分,剩下的权重百分比
--%>
<frameset rows="9%,*,10%">
        <frame src="${pageContext.request.contextPath}/admin/logo.jsp"/>
    <frameset cols="10%,*">
        <frame  src="${pageContext.request.contextPath}/admin/menu.jsp" />
        <%--frame框架标签
            name属性：给框架指定一个名称
        --%>
        <frame src="${pageContext.request.contextPath}/admin/main.jsp"  name="main"/>
    </frameset>
        <frame src="${pageContext.request.contextPath}/admin/footer.jsp"/>
</frameset>
<body>
<%--<c:if test="${not empty admin}">

    ${admin.name} ,登录了
</c:if>--%>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/24
  Time: 15:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>公共页面</title>
</head>
<body>

<!--
    描述：菜单栏
-->
<div class="container-fluid">
    <div class="col-md-4">
        <img src="${pageContext.request.contextPath}/img/logo2.png" />
    </div>
    <div class="col-md-5">
        <img src="${pageContext.request.contextPath}/img/header.png" />
    </div>
    <div class="col-md-3" style="padding-top:20px">
        <ol class="list-inline">
            <%--取出的user属性中没有内容--%>
            <c:if test="${empty user}">
                <li><a href="${pageContext.request.contextPath}/user?methodName=loginUI">登录</a></li>
                <!--${pageContext.request.contextPath} 通过pageContext域对象获取request bean属性同时通过
                request对象获取contextPath这个bean属性,获取上下文路径
                等价于pageContext.getRequest().getcontextPath()
                -->
                <li><a href="${pageContext.request.contextPath}/user?methodName=registerUI">注册</a></li>
                <li><a href="">购物车</a></li>
            </c:if>
            <%--取出的user属性的内容存在--%>
            <c:if test="${not empty user}">
                欢迎 &ensp;${user.name}&ensp;访问
                <li><a href="${pageContext.request.contextPath}/order?methodName=findOrderByPage&currentPage=1&pageSize=3">我的订单列表</a></li>
            </c:if>

        </ol>
    </div>
</div>
<!--
    描述：导航条
-->
<div class="container-fluid">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">首页</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul id="daohangUl" class="nav navbar-nav">
                    <%--	<li class="active"><a href="product_list.htm">手机数码<span class="sr-only">(current)</span></a></li>--%>

                    <%--<c:forEach items="${cList}" var="c">
                        <li><a href="#">${c.cname}</a></li>
                    </c:forEach>--%>
                    <%--<li><a href="#">电脑办公</a></li>
                    <li><a href="#">电脑办公</a></li>--%>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</div>
</body>
<script>
    $.get("${pageContext.request.contextPath}/category?methodName=findAllCategory",function (data) {

        //处理服务器响应过来的data
        //alert(data) ;

        //获取id="daohangUl"的标签对象
        var $ul =  $("#daohangUl") ;
        //遍历
        $(data).each(function(){

            //<li><a href="#">电脑办公</a></li>
            //Jquery的文档处理的dom操作
            $ul.append("<li><a href='${pageContext.request.contextPath}/product?methodName=findProductByPage&currentPage=1&pageSize=12&cid="+this.cid+"'>"+this.cname+"</a></li>") ;
        }) ;

    },"json") ;
</script>
</html>

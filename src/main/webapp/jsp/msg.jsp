<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2021/11/23
  Time: 14:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page  language="java" %>
<html>
<head>
    <title>消息提示页面</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" />
    <script src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
    <style>

        #tip{
            font-size: 25px;
            font-family: "黑体";
            text-align: center; /*文本居中*/

        }
    </style>

</head>
<body>

<%--导入header.jsp--%>
<%@include file="header.jsp"%>

<div id="tip">

    ${msg}
</div>
</body>
</html>

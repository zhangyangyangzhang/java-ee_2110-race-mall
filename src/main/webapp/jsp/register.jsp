<%@ page language="java"  pageEncoding="UTF-8"%>
<html>
	<head></head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Rice mall注册</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" />
		<script src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
<!-- 引入自定义css文件 style.css -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css"/>

<style>
  body{
   margin-top:20px;
   margin:0 auto;
 }
 .carousel-inner .item img{
	 width:100%;
	 height:300px;
 }
 .container .row div{ 
	 /* position:relative;
	 float:left; */
 }
 
font {
    color: #3164af;
    font-size: 18px;
    font-weight: normal;
    padding: 0 10px;
}
 </style>
	<script>

		//原生js写法,点击图片,后台刷新图片
		/*function imgClick(){

			var img = document.getElementById("img1");
			img.src = "${pageContext.request.contextPath}/checkCodeServlet?time="+new Date().getTime() ;
		}*/
		/*Jquery的方式将原生ajax的封装了
		* 1)$.ajax(url,[settings])
		* 2)$.get(url, [data], [callback], [type]) get请求的ajax
		* 3)$.post(url, [data], [callback], [type]) post请求的ajax
		* */
		//页面载入事件
		$(function () {

			//点击图片刷新
			//获取img图片标签对象
			//单击点击事件
			$("#img1").click(function(){

				//alert("触发图片点击事件") ;
				//改变img标签对象的src属性,重载加载后台新的验证码
				//图片是存在服务器端,点击图片的重新将之前的图片的缓存清除掉

				this.src = "${pageContext.request.contextPath}/checkCodeServlet?time="+new Date().getTime() ;

			}) ;


			//alert("触发页面载入事件") ;
            //用户输入完用户名,鼠标移出,失去焦点,触发失去焦点事件
            //通过jq的id选择器,获取标签对象,绑定事件
            $("#username").blur(function(){
                //alert("触发失去焦点事件") ;
                //获取到用户名
                var username = $(this).val() ; //获取输入框的内容
               // alert(username) ;
                //获取id="tip"的span标签对象
                var $tip = $("#tip") ;

                //Jquery的ajax
                //$.get("url","data","callback回调函数","type") ;
                $.get("${pageContext.request.contextPath}/user?methodName=checkUser&username="+username,function (data){
                            //data,服务器响应过来的数据
                         //alert(data) ;
                    //判断响应来的数据
                    if(data=="1"){
                        //用户已经存在了
                        //提示文本
                        $tip.html("太受欢迎了,请更换")
                        //设置文件
                        $tip.css("color","red")
                    }else{
                        //提示文本
                        $tip.html("恭喜您,可用")
                        //设置文件
                        $tip.css("color","green") ;
                    }


                },"text"); //服务器响应的数据格式:text:文本

            }) ;
		}) ;

	</script>
</head>

<body>



<%--静态导入--%>
<%@include file="header.jsp"%>





<div class="container" style="width:100%;background:url('${pageContext.request.contextPath}/image/regist_bg.jpg');">
<div class="row"> 

	<div class="col-md-2"></div>
	
	


	<div class="col-md-8" style="background:#fff;padding:40px 80px;margin:30px;border:7px solid #ccc;">
		<font>注册</font>REGISTER
		<form class="form-horizontal" style="margin-top:5px;" action="${pageContext.request.contextPath}/user?methodName=register" method="post">
			 <div class="form-group">
			    <label for="username" class="col-sm-2 control-label">用户名</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control" id="username" name="username" placeholder="请输入用户名">
                    <span id="tip"></span>
			    </div>
			  </div>
			   <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">密码</label>
			    <div class="col-sm-6">
			      <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="请输入密码">
			    </div>
			  </div>
			   <div class="form-group">
			    <label for="confirmpwd" class="col-sm-2 control-label">确认密码</label>
			    <div class="col-sm-6">
			      <input type="password" class="form-control"  id="confirmpwd" placeholder="请输入确认密码">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-6">
			      <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email">
			    </div>
			  </div>
			 <div class="form-group">
			    <label for="usercaption" class="col-sm-2 control-label">姓名</label>
			    <div class="col-sm-6">
			      <input type="text" class="form-control" name="name" id="usercaption" placeholder="请输入姓名">
			    </div>
			  </div>
			  <div class="form-group opt">  
			  <label for="inlineRadio1" class="col-sm-2 control-label">性别</label>  
			  <div class="col-sm-6">
			    <label class="radio-inline">
			  <input type="radio" name="sex" id="inlineRadio1" value="男"> 男
			</label>
			<label class="radio-inline">
			  <input type="radio" name="sex" id="inlineRadio2" value="女"> 女
			</label>
			</div>
			  </div>		
			  <div class="form-group">
			    <label for="date" class="col-sm-2 control-label">出生日期</label>
			    <div class="col-sm-6">
			      <input type="date" id="date" name="birthday" class="form-control"  >
			    </div>
			  </div>
			  
			  <div class="form-group">
			    <label  class="col-sm-2 control-label">验证码</label>
			    <div class="col-sm-3">
			      <input type="text" name="usercode" class="form-control"  >
			      
			    </div>
			    <div class="col-sm-2">
			     <%--<img  id="img1" onclick="imgClick()" src="${pageContext.request.contextPath}/checkCodeServlet"/>--%>
			     <img  id="img1"  src="${pageContext.request.contextPath}/checkCodeServlet"/>
			    </div>
			    
			  </div>
			 
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <input type="submit"  width="100" value="立即注册" name="submit" border="0"
				    style="background: url('${pageContext.request.contextPath}/image/register.gif') no-repeat scroll 0 0 #ffffff;
				    height:35px;width:100px;color:#000;background-color:#F7BE81">
			    </div>
			  </div>
			</form>
	</div>
	
	<div class="col-md-2"></div>
  
</div>
</div>

	  
	
	<div style="margin-top:50px;">
			<img src="${pageContext.request.contextPath}/image/footer.jpg" width="100%" height="78" alt="我们的优势" title="我们的优势" />
		</div>

		<div style="text-align: center;margin-top: 5px;">
			<ul class="list-inline">
				<li><a>关于我们</a></li>
				<li><a>联系我们</a></li>
				<li><a>招贤纳士</a></li>
				<li><a>法律声明</a></li>
				<li><a>友情链接</a></li>
				<li><a target="_blank">支付方式</a></li>
				<li><a target="_blank">配送方式</a></li>
				<li><a>服务声明</a></li>
				<li><a>广告声明</a></li>
			</ul>
		</div>
		<div style="text-align: center;margin-top: 5px;margin-bottom:20px;">
			Copyright &copy;2020-2030  Rice mall
		</div>

</body></html>






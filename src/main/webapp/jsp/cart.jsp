<%@ page language="java"
    pageEncoding="UTF-8"%>
<%--导入taglib指令 引入核心标签库--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>购物车</title>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" />
		<script src="${pageContext.request.contextPath}/js/jquery-3.4.1.min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- 引入自定义css文件 style.css -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" />
		<style>
			body {
				margin-top: 20px;
				margin: 0 auto;
			}
			
			.carousel-inner .item img {
				width: 100%;
				height: 300px;
			}
			
			.container .row div {
				/* position:relative;
	 float:left; */
			}
			
			font {
				color: #3164af;
				font-size: 18px;
				font-weight: normal;
				padding: 0 10px;
			}
		</style>
	</head>

	<body>
<%--静态导入--%>

<%@include file="header.jsp"%>

	<%--c:if判断--%>
    <c:if test="${empty cart}">
		 <span style="font-size: 30px;text-align: center;margin-left: 30px">亲,购物车空空如也~~,<a href="${pageContext.request.contextPath}/index">请购物</a></span>
	</c:if>
  <%--如果购物车不为空--%>
   <c:if test="${not empty cart}">
		<div class="container">
			<div class="row">

				<div style="margin:0 auto; margin-top:10px;width:950px;">
					<strong style="font-size:16px;margin:5px 0;">购物车</strong>
					<table class="table table-bordered">
						<tbody>
							<tr class="warning">
								<th>图片</th>
								<th>商品</th>
								<th>价格</th>
								<th>数量</th>
								<th>小计</th>
								<th>操作</th>
							</tr>

							<%--遍历当前的每一个购物车项--%>
							<c:forEach items="${cart.items}" var="item">
								<tr class="active">
									<td width="60" width="40%">
										<input type="hidden" name="id" value="22">
										<img src="${pageContext.request.contextPath}/${item.product.pimage}" width="70" height="60">
									</td>
									<td width="30%">
										<a target="_blank">${item.product.pname}</a>
									</td>
									<td width="20%">
										￥${item.product.shop_price}
									</td>
									<td width="10%">
										<input type="text" name="quantity" value="${item.count}" maxlength="4" size="10">
									</td>
									<td width="15%">
										<span class="subtotal">￥${item.subTotal}</span>
									</td>
									<td>
										<a href="javascript:void(0)" onclick="delCartItem('${item.product.pid}')" class="btn btn-danger">删除</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<div style="margin-right:130px;">
				<div style="text-align:right;">
					<em style="color:#ff6600;">
				登录后确认是否享有优惠&nbsp;&nbsp;
			</em> 赠送积分: <em style="color:#ff6600;">${cart.totalMoney}</em>&nbsp; 商品金额: <strong style="color:#ff6600;">￥${cart.totalMoney}</strong>
				</div>
				<div style="text-align:right;margin-top:10px;margin-bottom:10px;">
					<%--超链接---跳转到后台地址 --%>
					<a href="${pageContext.request.contextPath}/cart?methodName=clearCart" id="clear" class="btn btn-warning">清空购物车</a>
					<a href="${pageContext.request.contextPath}/order?methodName=addOrder">
						<input type="submit" width="100" value="提交订单" name="submit" border="0" style="background: url('${pageContext.request.contextPath}/images/register.gif') no-repeat scroll 0 0 #FE9A2E;
						height:35px;width:100px;color:white;">
					</a>
				</div>
			</div>

			</c:if>

		</div>

		<div style="margin-top:50px;">
			<img src="${pageContext.request.contextPath}/image/footer.jpg" width="100%" height="78" alt="我们的优势" title="我们的优势" />
		</div>

		<div style="text-align: center;margin-top: 5px;">
			<ul class="list-inline">
				<li><a>关于我们</a></li>
				<li><a>联系我们</a></li>
				<li><a>招贤纳士</a></li>
				<li><a>法律声明</a></li>
				<li><a>友情链接</a></li>
				<li><a target="_blank">支付方式</a></li>
				<li><a target="_blank">配送方式</a></li>
				<li><a>服务声明</a></li>
				<li><a>广告声明</a></li>
			</ul>
		</div>
		<div style="text-align: center;margin-top: 5px;margin-bottom:20px;">
			Copyright &copy;2020-2030  Rice mall
		</div>

	</body>

<script>
			/*点击删除,触发点击事件的函数*/
	function delCartItem(pid) {
		//alert(pid) ;
		//友好删除---提供确认提示框
		if(window.confirm("您忍心删除吗?")){
			//载入后台地址
			window.location.href = "${pageContext.request.contextPath}/cart?methodName=delCartItem&pid="+pid ;
		}
	}

</script>

</html>